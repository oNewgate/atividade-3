<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Rodape extends MY_Controller{
    
    public function index(){
        $this->load->view('cabecalho.php');
        $this->load->view('navbar.php');
        $this->load->view('rodape.php');
    }
}